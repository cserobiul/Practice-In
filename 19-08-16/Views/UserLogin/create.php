<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Create User</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<fieldset>
		<legend>Create A New User</legend>
		<form action="store.php" method="POST" accept-charset="utf-8">
			<table>
				<caption>User Information</caption>
				<thead><tr><th></th></tr></thead>
				<tbody>
					<tr>
						<td>Name</td>
						<td><input type="text" name="name" value=""></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type="email" name="email" value=""></td>
					</tr>
					<tr>
						<td>Password</td>
						<td><input type="password" name="password" value=""></td>
					</tr>
					<tr>
						<td><input type="reset" name="reset" value="Reset"></td>
						<td><input type="submit" name="submit" value="Submit Info"></td>
					</tr>

				</tbody>
			</table>
		</form>
	</fieldset>
	<h3><a href="index.php" title="All User List">All User List</a></h3>
</body>
</html>