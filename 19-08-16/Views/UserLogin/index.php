<?php 
include_once '../../vendor/autoload.php';
$dataObj = new Apps\UserLogin\Users();

$_REQUEST['active'] = 'yes';
$data = $dataObj->assign($_REQUEST)->index();

if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
	echo $_SESSION['msg'];
	unset($_SESSION['msg']);
}
if ($data>0) { ?>
	<table border="1" cellpadding="5">
		<caption>All User List</caption>
		<thead>
			<tr>
				<th>SL</th>
				<th>Name</th>
				<th>Email</th>
				<th colspan="3">Action</th>
			</tr>
		</thead>
		<tbody> 
			<?php 
			$sl=1;
			foreach ($data as $key) { ?>
				<tr>
					<td><?php echo $sl++ ?></td>
					<td><?php echo ucwords($key['name']) ?></td>
					<td><?php echo $key['email'] ?></td>
					<td><a href="show.php?id=<?php echo $key['uid'] ?>">View</a> </td>
					<td><a href="edit.php?id=<?php echo $key['uid'] ?>">Edit</a> </td>
					<td><a href="trash.php?id=<?php echo $key['uid'] ?>">Delete</a> </td>
				</tr>
				<?php }

				?>
			</tbody>
		</table>
		<?php }else{
			echo '<h2>Database Empty</h2>';
		}
		?>
		<h3><a href="create.php" title="Create New">Create New</a></h3>