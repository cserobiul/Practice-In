<?php 
namespace Apps\UserLogin;
use PDO;
use PDOException;

class Users{
	
	public $id;
	public $name;
	public $email;
	public $password;
	public $active;
	public $data;
	public $conn;


	public function __construct()
	{
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		try {
			$this->conn = new PDO('mysql:host=localhost;dbname=practices-in', 'root', '');
		} catch (PDOException $e) {
			echo 'Error: ' . $e->getMessage();
		}
	}

	public function assign($data){
		if (!empty($data['name'])) {
			$this->name = $data['name'];
		}
		if (!empty($data['email'])) {
			$this->email = $data['email'];
		}
		if (!empty($data['password'])) {
			$this->password = $data['password'];
		}

		if (!empty($data['id'])) {
			$this->id = $data['id'];
		}

		if ($data['active'] == 'yes') {			
			$this->active = 0;
		}else{
			$this->active = 1;
		}

		return $this;
		
	}
	

	public function store(){
		$create_at = date('Y-m-d H:i', time() - 6*4800);
		try {
			$query = "INSERT INTO tbl_users (name, email,password,uid,created_at) 
			VALUES(:name,:email,:password,:uid,:created_at)";
			$q = $this->conn->prepare($query);
			$q->execute(array(
				':name' => $this->name,
				':email' => $this->email,
				':password' => $this->password,
				':uid' => uniqid(),
				':created_at' => $create_at
				));
			$rowCount = $q->rowCount();
			return $rowCount;
		} catch (PDOException $e) {
			echo "Error: ".$e->getMessage();
		}
		return $this;
	}


	public function index(){
		//return $this->id;
		//die();
		try {
			$qry = "SELECT * FROM tbl_users WHERE is_delete = :id ORDER BY created_at DESC";
			$q = $this->conn->prepare($qry);
			$q->execute(array(
				':id' => $this->active
				));
			$rows = $q->rowCount();					
			if ($rows>0) {
				while ($rec = $q->fetch(PDO::FETCH_ASSOC)) {
					$this->data[] = $rec;
				}
				return $this->data;
			}else{
				return $rows;
			}
		} catch (PDOException $e) {
			echo "Error: ".$e->getMessage();		
		}

		
	}	
	public function show(){
		
	}
	public function update(){
		
	}
	public function trash(){
		
	}
	public function delete(){
		
	}


	
}


?>