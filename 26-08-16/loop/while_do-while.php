<?php

echo 'while: <br>';
$x = 1;

while($x <= 5) {
    echo "The number is: $x <br>";
    $x++;
} 

echo '<br> do while: <br>';

$y = 3;

do {
    echo "The number is: $y <br>";
    $y++;
}while ($y == 5);