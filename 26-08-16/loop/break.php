<?php
// break use on switch statement 
$searchEngin = "google";

switch ($searchEngin) {
    case "yahoo":
        echo "Your favorite search engin is yahoo!";
        break;
    case "bing":
        echo "Your favorite search engin is bing!";
        break;
    case "google":
        echo "Your favorite search engin is google!";
        break;
    default:
        echo "Your favorite search engin is neither yahoo, bing, nor google!";
}