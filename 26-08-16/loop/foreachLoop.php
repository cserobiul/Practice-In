<?php

echo 'Structure of foreach loop:<br>';
echo 'foreach ($array as $value) { <br>
    code to be executed;<br>
} <br>';

echo 'The Foreach loop works on arrays, and is used to loop through each key / value pair in an array.<br>';
$colors = array("red", "green", "blue", "yellow");

foreach ($colors as $value) {
    echo "$value <br>";
}

for($i=200;$i<=300;$i++){
    $array[] = $i;
 }    
 $data = array_reverse($array);

foreach ($data as $data) {
    echo $data;
    echo '<br>';
}